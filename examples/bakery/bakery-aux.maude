(
fmod BAKERY-PROPS is
 sorts Nat MBool .

 op 0 : -> Nat [ctor] .
 op s : Nat -> Nat [ctor] .
 ops True False : -> MBool [ctor] .
 ops _Mor_ : MBool MBool -> MBool [assoc comm] .
 sorts ProcIdle ProcWait Proc ProcIdleSet ProcWaitSet ProcSet .
 subsorts ProcIdle < ProcIdleSet .
 subsorts ProcWait < ProcWaitSet .
 subsorts ProcIdle ProcWait < Proc < ProcSet .
 subsorts ProcIdleSet < ProcWaitSet < ProcSet .
 op idle : -> ProcIdle [ctor] .
 op wait : Nat -> ProcWait [ctor] .
 op crit : Nat -> Proc [ctor] .
 op none : -> ProcIdleSet [ctor] .
 op __ : ProcIdleSet ProcIdleSet -> ProcIdleSet [ctor assoc comm id: none] .
 op __ : ProcWaitSet ProcWaitSet -> ProcWaitSet [ditto] .
 op __ : ProcSet ProcSet -> ProcSet [ditto] .

 sort State .
 op _:_`[_`] : Nat Nat ProcSet -> State [ctor] .

 ops init mutex good-state unique-tickets bounded-tickets : State -> [MBool] .

 var Is : ProcIdleSet .     var Ws : ProcWaitSet .     vars Ps Ps' : ProcSet .
 vars N M M' N' : Nat .     var P : Proc .  var B : MBool .

 eq True Mor B = True .
 eq False Mor B = B .

 eq init(0 : 0 [Is]) = True .

 eq mutex(N : M [Ws]) = True .
 eq mutex(N : M [crit(M') Ws]) = True .
 eq mutex(N : M [crit(M') crit(N') Ps]) = False .

 eq good-state(N : M [Ws]) = True .
 eq good-state(N : M [crit(M) Ws]) = True .
 eq good-state(N : M [crit(M') crit(N') Ps]) = False .

 eq bounded-tickets(N : M [Ps]) = sub-bag(tickets(Ps),tickets-below(N)) .
 eq unique-tickets(N : M [Ps]) = set?(tickets(Ps)) .

 sort NatBag .
 subsort Nat < NatBag .
 op mtbag : -> NatBag [ctor] .
 op __ : NatBag NatBag -> NatBag [ctor assoc comm id: mtbag] .

 op tickets-below : Nat -> NatBag .
 eq tickets-below(0) = mtbag .
 eq tickets-below(s(N)) = N tickets-below(N) .

 op tickets : ProcSet -> NatBag .
 eq tickets(Is) = mtbag .
 eq tickets(idle Ps) = tickets(Ps) .
 eq tickets(wait(N) Ps) = N tickets(Ps) .
 eq tickets(crit(N) Ps) = N tickets(Ps) .

 vars NB NB' NB'' : NatBag .

 op sub-bag : NatBag NatBag -> MBool .
 eq sub-bag(NB,NB NB') = True .

--- Equality enrichments
 op = : Nat Nat -> MBool [comm] .
  eq =(N,N) = True .            --- Added for confluence
  eq =(0,0) = True .
  eq =(s(N),0) = False .
  eq =(s(N),s(N')) = =(N,N') .
---a ceq [eqenrich] : N = N' if =(N,N') [nonexec] .

--- Innequality
 op <= : Nat Nat -> MBool .
 eq <=(0,N) = True .
 eq <=(N,N) = True .
 eq <=(s(N),s(M)) = <=(N,M) .
 eq <=(s(N),0) = False .

--- Bag membership
  op in? : Nat NatBag -> MBool .
  eq in?(N,mtbag) = False .
  eq in?(N,M NB) = =(N,M) Mor in?(N,NB) .

--- Some bags are sets 
  op set? : NatBag -> MBool .
  eq set?(mtbag) = True .
  ceq set?(N NB) = True if in?(N,NB) = False /\ set?(NB) = True .
---  ceq set?(N NB) = False if in?(N,NB) = True .
  eq set?(N N NB) = False .     --- Added for confluence

 rl N : M [idle Ps] => s(N) : M [wait(N) Ps] .

 rl N : M [wait(M) Ps] => N : M [crit(M) Ps] .

 rl N : M [crit(M) Ps] => N : s(M) [idle Ps] .

endfm)

eof

(red mutex( s(0) : 0 [crit(0) idle]) .)
(red mutex( s(0) : 0 [crit(0) crit(s(0))]) .)
(red good-state( s(0) : 0 [crit(0) crit(s(0))]) .)
(red bounded-tickets( s(0) : 0 [crit(0) crit(s(0))]) .)
(red bounded-tickets( s(s(0)) : 0 [crit(0) crit(s(0))]) .)
(red unique-tickets( s(s(0)) : 0 [crit(0) crit(s(0))]) .)
(red unique-tickets( s(s(0)) : 0 [crit(s(0)) crit(s(0))]) .)